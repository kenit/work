package main

import (
	"github.com/rivo/tview"
	"gitlab.com/kenit/work/core/database"
)

var db database.Engine

func main() {
	app := tview.NewApplication()
	pages := tview.NewPages()

	// Create and fill form for open/create DB
	openForm := tview.NewForm()
	openForm.SetBorder(true)
	openForm.SetTitle("Запуск программы")
	openForm.AddInputField("Файл данных:", "", 0, nil, nil)
	openForm.AddButton("Открыть/Создать", func() {
		db.Start(openForm.GetFormItemByLabel("Файл данных:").(*tview.InputField).GetText())
		pages.SwitchToPage("ProjectPage")
	})
	openForm.SetButtonsAlign(tview.AlignCenter)

	pages.AddPage("OpenPage", openForm, true, true)

	// Create/open project
	projectForm := tview.NewForm()
	projectForm.SetBorder(true)
	projectForm.SetTitle("Проект")
	projectForm.AddInputField("Имя проекта:", "", 0, nil, nil)
	projectForm.AddButton("Открыть проект", func() {
		field := projectForm.GetFormItemByLabel("Имя проекта:").(*tview.InputField).GetText()
		db.SetActiveProject(field)
	})
	projectForm.AddButton("Создать проект", func() {
		field := projectForm.GetFormItemByLabel("Имя проекта:").(*tview.InputField).GetText()
		db.CreateProject(field)
		db.SetActiveProject(field)
	})
	projectForm.SetButtonsAlign(tview.AlignCenter)

	pages.AddPage("ProjectPage", projectForm, true, false)

	// Create pages
	app.SetRoot(pages, true)
	app.SetFocus(pages)

	defer db.Stop()
	if err := app.Run(); err != nil {
		panic(err)
	}
}
