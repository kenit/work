package ship

// Base базовые данные по судну
type Base struct {
	Length     float64 // длинна судна в метрах
	Width      float64 // ширина в метрах
	Draft      float64 // осадка в метрах
	Height     float64 // высота борта
	Fullness   float64 // коэффициент полноты
	Deadweight float64 // дедвейт т/м3
	Speed      float64 // скорость в узлах
	Space      float64 // шпация в м
	Area       string  // район плавания

}

// Filling заполнение основных полей
func (b *Base) Filling(length, width, draft, height, fullness, deadweight, speed, space float64, area string) {
	b.Length = length
	b.Width = width
	b.Draft = draft
	b.Height = height
	b.Fullness = fullness
	b.Deadweight = deadweight
	b.Speed = speed
	b.Space = space
	b.Area = area
}

// BaseCoordinates привязка базовых координат к мидельшпангоуту
// положительные координаты в нос, отрицательные в корму
type BaseCoordinates struct {
	ZeroFrame              float64 // координата 0 шпангоута
	ForePeakperPendicular  float64 // координата носового перпендикуляра
	AfterPeakPerpendicular float64 // координата кормового перпендикуляра
	CentrOfMass            float64 // координата центра тяжести судна
}
// Filling заполнение основных полей
func (b *BaseCoordinates) Filling(zeroFrame, forePeakperPendicular, afterPeakPerpendicular, centrOfMass float64) {
	b.ZeroFrame = zeroFrame
	b.ForePeakperPendicular = forePeakperPendicular
	b.AfterPeakPerpendicular = afterPeakPerpendicular
	b.CentrOfMass = centrOfMass
}