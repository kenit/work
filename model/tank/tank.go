package tank

// Tank основные данные по цистерне
type Tank struct {
	Number       int64   // номер цистерны по танкоплану
	Name         string  // название цистерны
	Length       float64 // длинна цистерны в метрах
	Width        float64 // ширина цистерны в метрах
	Height       float64 // высота цистерны в метрах
	BallastFlag  bool    // признак балластной цистерны
	AirPipe      float64 // высота воздушной трубы в метрах
	Acceleration float64 // вертикальное ускорение м/с2
}

// BaseCoordinates координаты начала и конца цистерны
type BaseCoordinates struct {
	ForwardCoordinate  float64 // носовая координата в метрах
	BackwardCoordinate float64 // кормовая координата в метрах
}
