module gitlab.com/kenit/work

go 1.13

require (
	github.com/rivo/tview v0.0.0-20191121195645-2d957c4be01d
	go.etcd.io/bbolt v1.3.3
)
