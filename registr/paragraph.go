package registr

// P1_3_3_1 расчёт ускорений судна
// для расчёта az требуется y0 и x0, для ax и ay только z0
// возвращает массив ускорений [ax, ay, az]
// length - длинна судна в метрах
// width - ширина судна в метрах
// speed - скорость хода в узлах
// x0, y0, z0 - отстояние расчётной точки от центра тяжести в метрах
func P1_3_3_1(length, width, speed, phi, phir, x0, y0, z0 float64) (ax, ay, az float64) {
	theta := F1_3_3_1_5(length, phir)
	psi := F1_3_3_1_4(length, phi)
	tk, tb := F1_3_3_1_3(length, width, speed)
	accelx, accely, accelz := F1_3_3_1_2(length, phir, tk, tb, psi, theta, x0, y0, z0)
	ax = F1_3_3_1_1(accelx[0], accelx[1], accelx[2])
	ay = F1_3_3_1_1(accely[0], accely[1], accely[2])
	az = F1_3_3_1_1(accelz[0], accelz[1], accelz[2])
	return ax, ay, az
}

// P1_3_3_2 упрощённый расчёт вертикальных ускорений
// length - длинна судна в метрах
// x1 - отстояние от ближайшего перпендикуляра в метрах
// fore - признак носовой оконечности
func P1_3_3_2(length, x1 float64, fore bool) (az float64) {
	az = F1_3_3_1_6(length, x1, fore)
	return az
}
