package registr

import (
	"math"
)

// F1_3_3_1_1 определение суммарного ускорения
func F1_3_3_1_1(ac, ak, ab float64) (acceleration float64) {
	acceleration = math.Sqrt(math.Pow(ac, 2) + math.Pow(ak, 2) + 0.4*math.Pow(ab, 2))
	return acceleration
}

// F1_3_3_1_2 определение составных ускорений
// возвращает [acx, akx, abx], [acy, aky, aby], [acz, akz, abz]
// для расчёта Z требуется y0 и x0, для остальных двух только z0
func F1_3_3_1_2(length, phir, tk, tb, psi, theta, x0, y0, z0 float64) (accelerationx, accelerationy, accelerationz [3]float64) {
	// ускорения "c"
	accelerationx[0] = 0.1 * math.Pow((100/length), 1/3) * 9.81 * phir
	accelerationy[0] = 0.2 * math.Pow((100/length), 1/3) * 9.81 * phir
	accelerationz[0] = 0.2 * math.Pow((100/length), 1/3) * 9.81 * phir

	// ускорения "k"
	accelerationx[1] = math.Pow(2*math.Pi*tk, 2) * psi * z0
	accelerationy[1] = 0
	accelerationz[1] = math.Pow(2*math.Pi*tk, 2) * psi * x0

	// ускорения "b"
	accelerationx[2] = 0
	accelerationy[2] = math.Pow(2*math.Pi*tb, 2) * theta * z0
	accelerationz[2] = math.Pow(2*math.Pi*tb, 2) * theta * y0

	return accelerationx, accelerationy, accelerationz

}

// F1_3_3_1_3 расчёт периодов качки с допущениями первого приближения и не наливного судна
// length - длинна судна в метрах
// width - ширина судна в метрах
// speed - скорость хода в узлах
func F1_3_3_1_3(length, width, speed float64) (tk, tb float64) {
	tk = 0.8 * math.Sqrt(length) / (1 + 0.4*speed/math.Sqrt(length)*length/math.Pow10(3) + 0.4)

	// в первом приближении c = 0.8 и данных по h нет h = 0.07 * width
	c := 0.8
	h := 0.07 * width
	// судно не наливное
	tb = c * width / math.Sqrt(h)
	return tk, tb

}

// F1_3_3_1_4 расчёт угла деферента в радианах
// length - длинна судна в метрах
func F1_3_3_1_4(length, phi float64) (psi float64) {
	length = math.Max(length, 40)
	psi = phi * 0.23 / (1 + length/math.Pow10(2))
	return psi
}

// F1_3_3_1_5 расчёт угла крена в радианах
// length - длинна судна в метрах
func F1_3_3_1_5(length, phir float64) (theta float64) {
	length = math.Max(length, 40)
	theta = phir * 0.6 / (1 + 0.5*length/math.Pow10(2))
	return theta
}

// F1_3_3_1_6 упрощённый расчёт вертикальных ускорений
// length - длинна судна в метрах
// x1 - отстояние от ближайшего перпендикуляра в метрах
// fore - признак носовой оконечности
func F1_3_3_1_6(length, x1 float64, fore bool) (acceleration float64) {
	var (
		ka float64
	)
	x1 = math.Max(x1, 0) // отстояние от перпендикуляра всегда положительное
	if fore {
		ka = math.Max(1.6*(1-2.5*x1/length), 0)
	} else {
		ka = math.Max(0.5*(1-3.33*x1/length), 0)
	}

	acceleration = 9.81 * 0.9 / math.Pow(math.Max(length, 80), 1/3) * (1 + ka)

	return acceleration
}
