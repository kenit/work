package registr

import (
	"math"
)

// T1_3_1_5 таблица редуционных коэффициентов зоны R1, R2, R2-RSN, R2-RSN(4,5), R3-RSN, R3
// если зона не задана/ненайдена считается неограниченной зоны плавания phir = 1
func T1_3_1_5(length float64, area string) (phir float64) {

	if area == "R2" {
		phir = math.Min(1.25-0.25*length/math.Pow10(2), 1)
		return phir
	} else if area == "R2-RSN" {
		phir = 1 - 0.2*length/math.Pow10(2)
		return phir
	} else if area == "R2-RSN(4,5)" {
		phir = 0.94 - 0.19*length/math.Pow10(2)
		return phir
	} else if area == "R3-RSN" {
		phir = 0.86 - 0.18*length/math.Pow10(2)
		return phir
	} else if area == "R3" {
		phir = 0.86 - 0.18*length/math.Pow10(2)
		return phir
	}
	phir = 1
	return phir

}

// T1_4_4_3 таблица редуционных коэффициентов зоны R1, R2, R2-RSN, R2-RSN(4,5), R3-RSN, R3
// если зона не задана/ненайдена считается неограниченной зоны плавания phi = 1
func T1_4_4_3(length float64, area string) (phi float64) {

	if area == "R1" {
		phi = math.Min(1.1-0.23*length/math.Pow10(2), 1)
		return phi
	} else if area == "R2" {
		phi = 1 - 0.25*length/math.Pow10(2)
		return phi
	} else if area == "R2-RSN" {
		phi = 0.94 - 0.26*length/math.Pow10(2)
		return phi
	} else if area == "R2-RSN(4,5)" {
		phi = 0.92 - 0.29*length/math.Pow10(2)
		return phi
	} else if area == "R3-RSN" {
		phi = 0.71 - 0.22*length/math.Pow10(2)
		return phi
	} else if area == "R3" {
		phi = 0.6 - 0.2*length/math.Pow10(2)
		return phi
	}
	phi = 1
	return phi

}
