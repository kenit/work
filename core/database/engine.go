package database

import (
	"fmt"

	bolt "go.etcd.io/bbolt"
)

// Engine ядро работы с БД
type Engine struct {
	db            *bolt.DB
	activeProject string
}

// Start - запуск базы данных
func (e *Engine) Start(name string) error {
	db, err := bolt.Open(name, 0666, nil)
	if err != nil {
		return err
	}
	e.db = db
	return nil
}

// Stop - остановка базы
func (e *Engine) Stop() error {
	err := e.db.Close()
	if err != nil {
		return err
	}
	return nil
}

// SetActiveProject - установка активного проекта с которым будет вестись работа
func (e *Engine) SetActiveProject(name string) {
	e.activeProject = name
}

// CreateProject - создание проекта
func (e *Engine) CreateProject(name string) error {
	err := e.db.Update(func(tx *bolt.Tx) error {
		project, err := tx.CreateBucket([]byte(name))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		_, err = project.CreateBucket([]byte("ship"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		_, err = project.CreateBucket([]byte("tanks"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}

// SetShip - установка значения
func (e *Engine) SetShip(key, value []byte) error {
	err := e.db.Update(func(tx *bolt.Tx) error {
		ship := tx.Bucket([]byte(e.activeProject)).Bucket([]byte("ship"))
		err := ship.Put(key, value)
		if err != nil {
			return fmt.Errorf("set ship: %s", err)
		}
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}

// GetShip - получение значения
func (e *Engine) GetShip(key []byte) (value []byte, err error) {
	err = e.db.View(func(tx *bolt.Tx) error {
		ship := tx.Bucket([]byte(e.activeProject)).Bucket([]byte("ship"))
		value = ship.Get(key)

		return nil
	})
	if err != nil {
		return nil , err
	}
	return value, nil
}

// CreateTank - создание отдельной цистерны
func (e *Engine) CreateTank(id int64) error {
	err := e.db.Update(func(tx *bolt.Tx) error {
		_, err := tx.Bucket([]byte(e.activeProject)).Bucket([]byte("tanks")).CreateBucket([]byte(string(id)))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}

// SetTank - установка базового значения
func (e *Engine) SetTank(id int64, key, value []byte) error {
	err := e.db.Update(func(tx *bolt.Tx) error {
		tank := tx.Bucket([]byte(e.activeProject)).Bucket([]byte("tanks")).Bucket([]byte(string(id)))
		err := tank.Put(key, value)
		if err != nil {
			return fmt.Errorf("set tank: %s", err)
		}
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}

// GetTank - получение базового значения
func (e *Engine) GetTank(id int64, key []byte) (value []byte, err error) {
	err = e.db.View(func(tx *bolt.Tx) error {
		tank := tx.Bucket([]byte(e.activeProject)).Bucket([]byte("tanks")).Bucket([]byte(string(id)))
		value = tank.Get(key)
		return nil
	})
	if err != nil {
		return nil , err
	}
	return value, nil
}
